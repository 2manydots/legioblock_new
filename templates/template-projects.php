<?php

/* Template name: Projects Overview */
get_header(); ?>

    <main class="main">

        <?php //get_template_part('parts/breadcrumbs'); ?>        
        <?php if ( have_posts() ) : ?>
            <?php while ( have_posts() ) : the_post(); ?>
                <?php get_template_part('parts/top-header-image'); ?>
                <div class="row">
                    <div class="large-7 medium-9 small-10 columns">
                        <div class="breadcrumbs" typeof="BreadcrumbList" vocab="https://schema.org/">
                            <?php if(function_exists('bcn_display')):                            
                                bcn_display();
                            endif; ?>
                        </div>
                    </div>                    
                </div>
                <div class="row">
                    <div class="large-8 large-push-2 medium-10 medium-push-1 small-12 columns">
                        <!--<h1><?php the_title(); ?></h1>-->
                    </div>
                </div>
                <?php if( get_field('intro_text') ): ?>
                    <div class="row">
                        <div class="large-8 large-push-2 medium-10 medium-push-1 small-12 columns">
                            <div class="intro-text"><?php the_field('intro_text'); ?></div>
                        </div>
                    </div>
                <?php endif; ?>
                <?php get_template_part('parts/flexible-sections'); ?>
            <?php endwhile; ?>
        <?php endif; wp_reset_query(); ?>
        <?php
            $args = array(
                'post_type' => 'project',
                'orderby' => 'menu_order',
                'order'   => 'ASC',
                'posts_per_page'=>-1,
            );
            $terms_category = get_terms( array(
                'taxonomy' => 'project_category',
                'hide_empty' => true,
            ) );
            $terms_tags = get_terms( array(
                'taxonomy' => 'project_tag',
                'hide_empty' => true,
            ) );                            
        ?>
        <?php 
            if( isset($_GET["project_category"]) || isset($_GET["project_tag"]) ):
                $args['tax_query'] = array('relation'  => 'AND');
            endif;
            if( isset($_GET["project_category"]) ):
                $project_category_selected = $_GET["project_category"];
                if( $project_category_selected != '' ):
                    $args["tax_query"][] = array(
                        'taxonomy' => 'project_category',
                        'field'    => 'name',
                        'terms'    => $project_category_selected,
                        'operator' => 'IN'
                    );
                endif;                                
            endif;
            if( isset($_GET["project_tag"]) ):
                $project_tag_selected = $_GET["project_tag"];
                if( $project_tag_selected != '' ):
                    $args["tax_query"][] = array(
                        'taxonomy' => 'project_tag',
                        'field'    => 'name',
                        'terms'    => $project_tag_selected,
                        'operator' => 'IN'
                    );
                endif;                                
            endif;
        ?>
        <?php
            $query_projects = new WP_Query($args);
        ?>
        <div class="row">
            <div class="large-12 medium-12 small-12 columns">
                <div class="filters-wrapper">
                    <div class="single-filter">
                        <?php if( $first_filter_text = get_field('first_filter_text','option') ): ?>
                            <span class="filter-pre-text"><?php echo $first_filter_text; ?>:</span>
                        <?php endif; ?>
                        <!--<span class="filter-pre-text">Toon:</span>-->
                        <select class="filter-project filter-project-category" name="project_category">
                            <option value=""><?php the_field('all_choices_text','option'); ?></option>
                            <?php foreach( $terms_category as $term_category ): ?>
                                <option <?php if( $project_category_selected == $term_category->name ): echo 'selected'; endif; ?> value="<?php echo $term_category->name; ?>"><?php echo $term_category->name; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="single-filter">
                        <?php if( $second_filter_text = get_field('second_filter_text','option') ): ?>
                            <span class="filter-pre-text"><?php echo $second_filter_text; ?>:</span>                            
                        <?php endif; ?>
                        <!--<span class="filter-pre-text">Filter op:</span>-->
                        <select class="filter-project filter-project-sectors" name="project_tag">
                            <option value=""><?php the_field('all_choices_text','option'); ?></option>
                            <?php foreach( $terms_tags as $terms_tag ): ?>
                                <option <?php if( $project_tag_selected == $terms_tag->name ): echo 'selected'; endif; ?> value="<?php echo $terms_tag->name; ?>"><?php echo $terms_tag->name; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>                            
                </div>
            </div>
        </div>
        <?php if ( $query_projects->have_posts() ) : ?>
            <section class="projects-list-secion">                
                <div class="row">
                    <?php while ( $query_projects->have_posts() ) : $query_projects->the_post(); ?>
                        <div class="large-4 medium-6 small-12 columns column-wrapper">
                            <div class="inner-wrapper">
                                <div class="image-wrapper">
                                    <?php if( $project_top_image = get_field('top_header_image') ): ?>
                                        <img src="<?php echo $project_top_image['sizes']['ctb-thumb']; ?>" alt="image">
                                    <?php else: ?>
                                        <?php if( has_post_thumbnail() ): ?>
                                            <?php the_post_thumbnail('ctb-thumb'); ?>
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </div>
                                <div class="title-wrapper">                                   
                                    <h4 class="small-title"><?php the_title(); ?></h4>
                                    <?php if( $sub_title = get_field('sub_title') ): ?>
                                        <div class="sub-title">
                                            <?php echo $sub_title; ?>
                                        </div>
                                    <?php endif; ?>
                                </div>
                                <a href="<?php the_permalink(); ?>"class="link"></a>
                            </div>
                        </div>
                    <?php endwhile; ?>
                </div>                
            </section>
        <?php else: ?>
            <div class="row">
                <div class="large-12 medium-12 small-12 columns">
                    <p>Empty Results</p>
                </div>                    
            </div>
        <?php endif; wp_reset_query(); ?>
        
    </main>

<?php get_footer(); ?>