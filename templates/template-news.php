<?php

/* Template name: News Overview */
get_header(); ?>

    <main class="main">

        <?php //get_template_part('parts/breadcrumbs'); ?>        
        <?php if ( have_posts() ) : ?>
            <?php while ( have_posts() ) : the_post(); ?>
                <?php get_template_part('parts/top-header-image'); ?>
                <div class="row">
                    <div class="large-7 medium-9 small-10 columns">
                        <div class="breadcrumbs" typeof="BreadcrumbList" vocab="https://schema.org/">
                            <?php if(function_exists('bcn_display')):                            
                                bcn_display();
                            endif; ?>
                        </div>
                    </div>                    
                </div>
                <div class="row">
                    <div class="large-8 large-push-2 medium-10 medium-push-1 small-12 columns">
                        <!--<h1><?php the_title(); ?></h1>-->
                    </div>
                </div>
                <?php if( get_field('intro_text') ): ?>
                    <div class="row">
                        <div class="large-8 large-push-2 medium-10 medium-push-1 small-12 columns">
                            <div class="intro-text"><?php the_field('intro_text'); ?></div>
                        </div>
                    </div>
                <?php endif; ?>
            <?php endwhile; ?>
        <?php endif; wp_reset_query(); ?>

        <?php $arg = array(
            'post_type'	    => 'nieuws',
            'posts_per_page'    => -1
        );
        $the_query = new WP_Query( $arg ); ?>

        <?php if ( $the_query->have_posts() ) : ?>
            <section class="projects-list-secion">                
                <div class="row">
                    <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                        <div class="large-4 medium-6 small-12 columns column-wrapper">
                            <div class="inner-wrapper">
                                <div class="image-wrapper">
                                        <?php if( has_post_thumbnail() ): ?>
                                            <?php the_post_thumbnail('ctb-thumb'); ?>
                                        <?php else: ?>
                                            <?php if( $project_top_image = get_field('top_header_image') ): ?>
                                                <img src="<?php echo $project_top_image['sizes']['ctb-thumb']; ?>" alt="image">
                                            <?php endif; ?>
                                        <?php endif; ?>
                                </div>
                                <div class="title-wrapper">                                   
                                    <h4 class="small-title"><?php the_title(); ?></h4>                                    
                                    <?php if( $sub_title = get_field('sub_title') ): ?>
                                        <div class="sub-title">
                                            <?php echo $sub_title; ?>
                                        </div>
                                    <?php endif; ?>
                                </div>
                                <a href="<?php the_permalink(); ?>"class="link"></a>
                            </div>
                        </div>
                    <?php endwhile; ?>
                </div>                
            </section>
        <?php else: ?>
            <div class="row">
                <div class="large-12 medium-12 small-12 columns">
                    <p>Empty Results</p>
                </div>                    
            </div>
        <?php endif; wp_reset_query(); ?>

        <?php get_template_part('parts/flexible-sections'); ?>
        
    </main>

<?php get_footer(); ?>