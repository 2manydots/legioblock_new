<?php
/* Template name: Home */

get_header(); ?>

    <main class="main">
        
        <?php get_template_part('parts/top-header-image'); ?>
        <?php get_template_part('parts/home-page-top'); ?>
        <?php get_template_part('parts/flexible-sections'); ?>
        <div class="row">
            <div class="medium-12 columns">

                <?php if ( have_posts() ) : ?>

                    <?php while ( have_posts() ) : the_post(); ?>

                        <div class="row">
                            <div class="medium-12 columns">
                                <!--<h1><?php the_title(); ?></h1>-->
                                <?php //the_content(); ?>
                            </div>
                        </div>

                    <?php endwhile;

                    //get_template_part('parts/pagination');

                else :

                    //get_template_part('parts/no-content');

                endif; ?>

            </div>
        </div>
    </main>

<?php get_footer(); ?>