(function( $ ) {
    $(function() {

        // toggle menu icon
        $('.menu-icon').on('click', function () {
          $(this).addClass('opened');
        });
    });
    $(document).ready(function(){
        //Don't follow current language
        $('.current-language').on('click', function(e){
            e.preventDefault();
        });
        //Toggle Languages        
        $('.language-select').on('click', function(e){
            if( $(this).hasClass('opened') ){
                $('.other-languages').hide();
                $('.language-select').removeClass('opened');
            }
            else {
                $(this).addClass('opened');
                $('.other-languages').show();
            }
        });
        $(document).on('click touchstart', function(event){
            if(!$(event.target).closest('.language-select').length) {                
                if($('.other-languages').is(":visible")) {
                    $('.other-languages').hide();
                    $('.language-select').removeClass('opened');
                }
            }
        });
        $(window).load(function(){
            //match height
            $('.eq-height-text').matchHeight();
            $('.step-wrapper').matchHeight();
            $('.featured-pages-section .inner-wrapper').matchHeight();
            $('.table-column').matchHeight();
            $('.usps-section .inner-wrapper').matchHeight();
            $('.featured-pages-section .single-column').matchHeight();
            $('.projects-list-secion .inner-wrapper .title-wrapper').matchHeight();
            $('.eq-image-cols').matchHeight();
        });
        //Tabs
        $('.tabs-control .element-wrapper:first-child').addClass('active');
        $('.tabs-control .element-wrapper').on('click', function(e){
            e.preventDefault();
            if( !$(this).hasClass('active') ){
                $(this).siblings().removeClass('active');
                $(this).addClass('active');
                var tabNumber = $(this).attr('data-tabCount');
                $('.tab-content').hide();
                $(".tab-content[data-tabcount='"+tabNumber+"']").show();
                if(history.pushState) {
                    history.pushState(null, null, '#'+$(this).attr('id'));
                }
                else {
                    location.hash = '#'+$(this).attr('id');
                }
                //window.location.hash = $(this).attr('id');
            }
        });
        //Slider
        $('.main-slider').slick({
            infinite: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            dots: true,
            speed: 500,
            fade: true,
            cssEase: 'linear',
            prevArrow: '<button type="button" class="slick-prev"></button>',
            nextArrow: '<button type="button" class="slick-next"></button>',
        });
        //Category Filter
        $('.filter-project').on('change', function(){
            //console.log();            
            var url = window.location.origin+window.location.pathname;            
            // update url
		url += '?';
                $('.filter-project').each(function(){
                    var name = $(this).attr("name");
                    var value = $(this).val();
                    if( value.length > 0) {
                        url += name + '=' + value + '&';
                    }                    
                });                
            // remove last &
		url = url.slice(0, -1);
            // reload page
		window.location.replace( url );
        });        
        var filterValues = window.location.search;
        var pos1 = filterValues.indexOf("project_category");
        var pos2 = filterValues.indexOf("project_tag");
        //check if filters was set
        if( pos1 >= 0 || pos2 >= 0 ) {
            //scroll to filter results
            if ($('.projects-list-secion').length) {
                $('html,body').animate({
                        scrollTop: $('.projects-list-secion').offset().top+30
                }, 1000);
                //return false;
            }
        }
        //Headroom        
        var myElement = document.querySelector("header.headroom");
        var headroom  = new Headroom(myElement, {
            offset : 205,
            tolerance: {
                down : 10,
                up : 20
            }
        });
        headroom.init();
        //Content Offset
        function doContentOffset() {
            var headerHeight = $('.header').outerHeight();
            if( $('#wpadminbar').length > 0 ){
                var headerAdminHeight = $('#wpadminbar').outerHeight();                
                $('.header').css('top', headerAdminHeight+'px');
                //headerHeight = headerHeight ;
            }
            $('.main').css({"margin-top":headerHeight+"px"});
        }
        doContentOffset();
        $(window).resize(function(){
            doContentOffset();
        });
        //Mobile menu
        $(".menu-toggler").on('click', function(e){
            e.preventDefault();
            $('.mobile-menu').toggleClass("active").slideToggle();
            $('.nav-icon').toggleClass('open');
        });
        //home page top section
        function fitContentBlock(){
            var innerHeightWrapper = $('.homepage-top-section .content-wrapper').height();
            var innerHeightContent = $('.homepage-top-section .content-inner-wrapper').height();
            if( innerHeightWrapper < innerHeightContent ) {
                $('.homepage-top-section .content-wrapper').height(innerHeightContent+30);
            }
            else {
                if( $(window).width() < 1024 ){
                    $('.homepage-top-section .content-wrapper').height(innerHeightContent+30);
                }
                else {
                    $('.homepage-top-section .content-wrapper').height('');
                }
            }
        }
        fitContentBlock();
        $(window).resize(function(){
            fitContentBlock();
        });
        //scroll to anchors
        $('a[href*=#]:not([href=#])').click(function(e) {            
            if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
                var target = $(this.hash);
                var headerBoxHeight_ = $('.header').outerHeight();
                target = target.length ? target : $('[name=' + this.hash.slice(1) +']');                    
                if (target.length) {
                    $('html,body').animate({
                            scrollTop: target.offset().top - headerBoxHeight_
                    }, 1000);
                    return false;
                }
            }
        });
        //mobile menu sub-items
        $( ".menu-item-has-children .arrow" ).on('click', function(){
            $(this).siblings('.sub-menu').slideToggle();
            $(this).parent().toggleClass('active-sub');
        });
    });
    $(window).load(function(){
        //links to anchors on window load
        var headerBoxHeight_ = $('.header').outerHeight();
        var hash = window.location.hash;
        if(hash.length > 0) {
            var target = $('[id=' + window.location.hash.slice(1) +']');
            console.log(target);
            target.click();
            $('html,body').animate({
                scrollTop: target.offset().top - headerBoxHeight_
            }, 1000);            
        }        
    });
    //Pop Up    
    $(window).load(function(){
        var popUp = $('.pop-up');
        
        if(popUp.length > 0 ) {
            var pxoffset = popUp.attr('data-pxftop');
            var time     = popUp.attr('data-timeout');            
            if( parseInt(pxoffset) > 0 ){
                if( !popUp.hasClass('pop-animate') ){
                    $(window).scroll(function() {
                        if ($(window).scrollTop() > parseInt(pxoffset)) {
                            popUp.addClass('pop-animate').show();
                        }
                    });
                }                
            }
            else if( parseInt(time) > 0 ){
                setTimeout(function(){
                    popUp.addClass('pop-animate').show();
                }, parseInt(time));
            }
            else {
                popUp.addClass('pop-animate').show();
            }           
        }
    });
    $('.pop-up .close-btn').click(function(){
        $('.pop-up').hide();
    });
})(jQuery);