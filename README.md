# 2manydots WordPress Grunt Boilerplate

An example of the boilerplate can be found [here](http://boilerplate.flywheelsites.com).

## Changelog
Current version: 0.4

| Version | Comment                             | Author                          |
| ------- | ----------------------------------- | ------------------------------- |
| 0.4     | Several improvements and bugfixes   | Patrick Leijser & Oleg Kozlov   |
| 0.3     | Improvements for the plugin         | Patrick Leijser                 |
| 0.2     | Suggestions Victor merged           | Patrick Leijser                 |
| 0.1     | Initial GRUNT Boilerplate           | Patrick Leijser                 |