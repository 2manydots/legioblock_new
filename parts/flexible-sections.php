<?php if( have_rows('flexible_sections') ): $section_counter = 1; ?>
    <?php while( have_rows('flexible_sections') ): the_row(); ?>
        <?php if( get_row_layout() == 'two_columns_text' ): ?>
            <?php $separate_line = get_sub_field('separate_line'); ?>
            <!-- Two Columns Content -->
            <section class="two-columns-text" id="section-<?php echo $section_counter; ?>">
                <div class="row">
                    <?php if ( $left_column = get_sub_field('left_column') ): ?>
                    <div class="large-6 medium-6 small-12 columns left-column eq-height-text" <?php if($separate_line): ?>style="border-right: 1px solid #ed9d19;"<?php endif; ?>>
                            <div class="content">
                                <?php echo $left_column; ?>
                            </div>
                        </div>                        
                    <?php endif; ?>
                    <?php if ( $right_column = get_sub_field('right_column') ): ?>
                        <div class="large-6 medium-6 small-12 columns right-column eq-height-text">
                            <div class="content">
                                <?php echo $right_column; ?>
                            </div>
                        </div>                        
                    <?php endif; ?>
                </div>
            </section>
            <?php $section_counter++; ?>
            <!-- End Two Columns Content -->
        <?php elseif( get_row_layout() == 'steps_section' ): ?>
            <!-- Steps Section -->
            <section class="steps-section" id="section-<?php echo $section_counter; ?>">
                <?php if( $title = get_sub_field('title') ): ?>
                    <div class="row">
                        <div class="large-12 medium-12 small-12 columns">
                            <h3 class="section-title"><?php echo $title; ?></h3>
                        </div>
                    </div>
                <?php endif; ?>
                <?php if( have_rows('steps') ): ?>
                    <div class="row">
                        <?php while( have_rows('steps') ): the_row(); ?>
                            <div class="large-3 medium-6 small-12 columns step-column">
                                <div class="step-wrapper">
                                    <?php if( get_sub_field('image') ): ?>
                                        <div class="image-wrapper">
                                            <img src="<?php the_sub_field('image'); ?>" alt="step-image">
                                        </div>
                                    <?php endif; ?>
                                    <div class="title-wrapper">
                                        <?php if( get_sub_field('number') ): ?>
                                            <span class="number">
                                                <?php the_sub_field('number'); ?>
                                            </span>
                                        <?php endif; ?>
                                        <?php if( get_sub_field('title') ): ?>
                                            <h4><?php the_sub_field('title'); ?></h4>
                                        <?php endif; ?>
                                    </div>
                                </div>
                                <div class="arrow">
                                    <img class="svg-img" src="<?php echo get_template_directory_uri(); ?>/images/oval-arrow.svg" alt="arrow-oval">
                                </div>
                            </div>
                        <?php endwhile; ?>
                    </div>
                <?php endif; ?>
            </section>
            <?php $section_counter++; ?>
            <!-- End Steps Section -->
        <?php elseif( get_row_layout() == 'call_to_action_section' ): ?>
            <!-- Call to action section -->
            <section class="call-to-action-section" id="section-<?php echo $section_counter; ?>">
                <div class="row">
                    <div class="large-12 medium-12 small-12 columns">
                        <div class="pre-wrapper">
                            <div class="row">
                                <div class="large-8 large-push-2 medium-10 medium-push-1 small-12 columns">
                                    <div class="inner-wrapper">
                                        <?php if( get_sub_field('content') ): ?>
                                            <div class="content">
                                                <?php the_sub_field('content'); ?>
                                            </div>
                                        <?php endif; ?>                                        
                                        <?php if( have_rows('buttons') ): ?>                                            
                                            <div class="btn-wrapper">
                                                <?php while( have_rows('buttons') ): the_row(); ?>
                                                    <?php
                                                        $button_text = get_sub_field('button_text');
                                                        $button_url  = get_sub_field('button_url');
                                                    ?>
                                                    <?php if( $button_text && $button_url ): ?>
                                                        <a href="<?php echo $button_url; ?>" class="btn"><?php echo $button_text; ?></a>
                                                    <?php endif; ?>
                                                <?php endwhile; ?>                                                
                                            </div>                            
                                        <?php endif; ?>                                        
                                    </div>
                                </div>
                            </div>
                        </div>                        
                    </div>
                </div>                                
            </section>
            <?php $section_counter++; ?>
            <!-- End Call to action section -->
        <?php elseif( get_row_layout() == 'featured_pages_section' ): ?>
            <!-- Featured Pages Section -->
            <section class="featured-pages-section" id="section-<?php echo $section_counter; ?>">
                <?php if( have_rows('featured_pages') ): ?>
                    <div class="row">
                        <?php while( have_rows('featured_pages') ): the_row(); ?>
                            <div class="large-4 medium-6 small-12 columns single-column">
                                <div class="inner-wrapper">
                                    <?php if( get_sub_field('icon') ): ?>
                                        <div class="image-wrapper">
                                            <img src="<?php the_sub_field('icon'); ?>" alt="icon">
                                        </div>
                                    <?php endif; ?>
                                    <?php if( get_sub_field('title') ): ?>                                        
                                        <h4 class="title"><?php the_sub_field('title'); ?></h4>
                                    <?php endif; ?>
                                    <?php if( get_sub_field('description') ): ?>                                        
                                        <div class="content"><?php the_sub_field('description'); ?></div>
                                    <?php endif; ?>
                                    <?php if( get_sub_field('link') ): ?>
                                        <a href="<?php the_sub_field('link'); ?>" class="link"></a>                                    
                                    <?php endif; ?>
                                </div>                                
                            </div>
                        <?php endwhile; ?>
                    </div>
                <?php endif; ?>
            </section>
            <?php $section_counter++; ?>
            <!-- End Featured Pages Section -->
        <?php elseif( get_row_layout() == 'text_with_image_section' ): ?>
            <!-- Text With Image Section -->
            <section class="text-with-image-section" id="section-<?php echo $section_counter; ?>">
                <div class="row">
                    <?php $content_right = get_sub_field('content_right'); ?>
                    <?php if( get_sub_field('content') ): ?>
                        <div class="large-6 medium-6 small-12 columns <?php if($content_right): ?>large-push-6 medium-push-6<?php endif; ?>">
                            <div class="content">
                                <?php the_sub_field('content'); ?>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php if( get_sub_field('image') ): ?>
                        <div class="large-6 medium-6 small-12 columns <?php if($content_right): ?>large-pull-6 medium-pull-6<?php endif; ?>">
                            <div class="content">
                                <img src="<?php the_sub_field('image'); ?>" alt="img">                                
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
            </section>
            <?php $section_counter++; ?>
            <!-- End Text With Image Section -->
        <?php elseif( get_row_layout() == 'advisor_contact_section' ): ?>
            <!-- Advisor Contact Section -->
            <section class="advisor-contact-section" id="section-<?php echo $section_counter; ?>">
                <div class="row">
                    <div class="large-12 medium-12 small-12 columns">
                        <div class="pre-wrapper">
                            <div class="bricks-wrapper"></div>
                            <div class="row">
                                <?php if( $image = get_sub_field('image') ): ?>
                                    <div class="large-2 large-push-1 medium-5 medium-push-1 small-12 columns">
                                        <img src="<?php echo $image['sizes']['contact-advisor-thumb'];?>" alt="person">
                                    </div>
                                <?php endif; ?>
                                <div class="large-6 large-push-2 medium-6 small-12 columns">
                                    <?php if( get_sub_field('content') ): ?>
                                        <div class="content">
                                            <?php the_sub_field('content'); ?>
                                        </div>
                                    <?php endif; ?>
                                    <?php
                                        $telephone_number_text = get_sub_field('telephone_number_text');
                                        $telephone_number_link = get_sub_field('telephone_number_link');
                                        $email_text            = get_sub_field('email_text');
                                        $email_link            = get_sub_field('email_link');
                                    ?>
                                    <?php if( ($telephone_number_text && $telephone_number_link) || ($email_text && $email_link) ): ?>
                                        <div class="btns-wrapper">
                                            <?php if( $telephone_number_text && $telephone_number_link ): ?>
                                                <a href="tel:<?php echo $telephone_number_link; ?>"class="btn tel"><?php echo $telephone_number_text; ?></a>
                                            <?php endif; ?>
                                            <?php if( $email_text && $email_link ): ?>
                                                <a href="mailto:<?php echo $email_link; ?>"class="btn email"><?php echo $email_text; ?></a>
                                            <?php endif; ?>
                                        </div>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <?php $section_counter++; ?>
            <!-- End Advisor Contact Section -->
        <?php elseif( get_row_layout() == 'text_section' ): ?>
            <!-- Text Section -->
            <section class="text-section" id="section-<?php echo $section_counter; ?>">
                <?php if( get_sub_field('text') ): ?>
                    <div class="row">
                        <div class="large-8 large-push-2 medium-10 medium-push-1 small-12 columns">
                            <div class="content">
                                <?php the_sub_field('text'); ?>
                            </div>
                        </div>
                    </div>
                <?php endif;?>
                <?php
                    $button_text = get_sub_field('button_text');
                    $button_url  = get_sub_field('button_url');
                ?>
                <?php if( $button_text && $button_url ): ?>
                    <div class="row">
                        <div class="large-8 large-push-2 medium-10 medium-push-1 small-12 columns">
                            <a href="<?php echo $button_url; ?>" class="btn"><?php echo $button_text; ?></a>
                        </div>                            
                    </div>
                <?php endif;?>
            </section>
            <?php $section_counter++; ?>
            <!-- End Text Section -->
        <?php endif; ?>
        <?php if( get_row_layout() == 'catalog_section' ): ?>
            <!-- Catalog Section -->
            <section class="catalog-section" id="section-<?php echo $section_counter; ?>">
                <?php if( get_sub_field('title') ): ?>
                    <div class="row">
                        <div class="large-12 medium-12 small-12 columns">
                            <h3 class="section-title"><?php the_sub_field('title'); ?></h3>
                        </div>
                    </div>
                <?php endif; ?>
                <div class="row">
                    <div class="large-12 medium-12 small-12 columns">
                        <div class="tabs-control">
                            <?php if( have_rows("tabs") ): $tabs_count = 1; ?>
                                <div class="elements-wrapper">
                                    <?php while( have_rows("tabs") ): the_row();?>
                                        <?php if( $tab_title = get_sub_field("title") ): ?>
                                            <a class="element-wrapper" data-tabcount='<?php echo $tabs_count; ?>' id="tab-el-<?php echo $tabs_count; ?>">
                                                <?php echo $tab_title; ?>
                                            </a>
                                        <?php endif; ?>                                
                                    <?php $tabs_count++; endwhile; ?>
                                </div>                        
                            <?php endif; wp_reset_query(); ?>
                        </div>
                    </div>
                </div>
                
                <?php if( have_rows("tabs") ): $tabs_count = 1; ?>
                    <div class="tabs-content-wrapper">
                        <?php while( have_rows("tabs") ): the_row();?>
                            <div class="tab-content" data-tabcount='<?php echo $tabs_count; ?>' id="tab-content-<?php echo $tabs_count; ?>">
                                <?php if( have_rows('tab_section') ): ?>
                                    <?php while( have_rows('tab_section') ): the_row(); ?>
                                        
                                        <?php 
                                            $project_select       = get_sub_field('project_page');
                                            $project_custom_title = get_sub_field('project_custom_title');
                                            $project_custom_image = get_sub_field('project_custom_image');
                                            $project_custom_link  = get_sub_field('project_custom_link');                                            
                                        ?>
                                
                                        <div class="row">
                                            <div class="large-9 medium-9 small-12 columns">
                                                <!-- Items Rows -->                                                                                
                                                <?php if( have_rows('items') ): ?>
                                                    <div class="row">
                                                        <?php while( have_rows('items') ): the_row(); ?>

                                                            <div class="large-4 medium-4 small-8 columns table-column">
                                                                <div class="pre-table">
                                                                    <div class="pre-table-cell">
                                                                        <?php the_sub_field('title'); ?>
                                                                    </div>
                                                                </div>                                                                
                                                            </div>

                                                            <div class="large-4 medium-4 small-8 columns table-column">
                                                                <div class="pre-table">
                                                                    <div class="pre-table-cell">
                                                                        <?php the_sub_field('weight'); ?>
                                                                    </div>                                                                        
                                                                </div>                                                                
                                                            </div>

                                                            <div class="large-4 medium-4 small-4 columns table-column">
                                                                <?php if( get_sub_field('image') ): ?>
                                                                    <img class="info-image" src="<?php the_sub_field('image'); ?>" alt="img-prev">
                                                                <?php endif; ?>                                                            
                                                            </div>

                                                        <?php endwhile; ?>
                                                    </div>
                                                <?php endif; ?>
                                                <!-- Items Rows -->
                                            </div>
                                            <div class="large-3 medium-3 small-12 columns project-pre-wrapper">
                                                
                                                <?php if( $project_select ): $post = $project_select; setup_postdata( $post ); ?>
                                                    <div class="inner-wrapper">
                                                        <div class="image-wrapper">
                                                            <?php if( $project_custom_image ): ?>
                                                                <img src="<?php echo $project_custom_image['sizes']['ctb-thumb']; ?>" alt="image">
                                                            <?php elseif( $imageTop = get_field('top_header_image') ): ?>
                                                                <img src="<?php echo $imageTop['sizes']['ctb-thumb']; ?>" alt="image">
                                                            <?php else: ?>                                                            
                                                                <?php if( has_post_thumbnail() ): ?>
                                                                    <?php the_post_thumbnail('ctb-thumb'); ?>
                                                                <?php endif; ?>
                                                            <?php endif; ?>
                                                        </div>
                                                        <div class="title-wrapper">
                                                            <?php if( $project_custom_title ): ?>
                                                                <h6 class="small-title"><?php echo $project_custom_title; ?></h6>
                                                            <?php else: ?>
                                                                <h6 class="small-title"><?php the_title(); ?></h6>
                                                            <?php endif; ?>
                                                        </div>
                                                        <?php if( $project_custom_link ): ?>
                                                            <a href="<?php echo $project_custom_link; ?>" class="link"></a>
                                                        <?php else: ?>
                                                            <a href="<?php the_permalink(); ?>"class="link"></a>
                                                        <?php endif; ?>                                                        
                                                    </div>
                                                <?php wp_reset_postdata(); endif; ?>                                                
                                            </div>
                                            <div class="large-12 columns">
                                                <hr>
                                            </div>
                                        </div>                                        
                                        
                                    <?php endwhile; ?>
                                <?php endif; ?>
                                
                            </div>                        
                        <?php $tabs_count++; endwhile; ?>
                    </div>                    
                <?php endif; wp_reset_query(); ?>                
            </section>
            <?php $section_counter++; ?>
            <!-- End Catalog Section -->
        <?php elseif( get_row_layout() == 'quote_section' ): ?>
            <!-- Quote Section-->
            <section class="quote-section" id="section-<?php echo $section_counter; ?>">
                <div class="row">
                    <div class="large-12 medium-12 small-12 columns">
                        <div class="pre-wrapper">
                            <div class="bricks-wrapper"></div>
                            <div class="row">
                                <?php $position = get_sub_field('author_image_position') == 'right' ? true : false; ?>
                                <?php if( $image = get_sub_field('autor_image') ): ?>
                                    <div class="large-2 <?= $position ? 'large-push-8 medium-push-8' : 'large-push-1 medium-push-1'; ?> medium-5 small-12 columns">
                                        <img src="<?php echo $image['sizes']['contact-advisor-thumb'];?>" alt="person">
                                    </div>
                                <?php endif; ?>
                                <div class="large-6 <?= $position ? 'medium-pull-4 large-pull-1' : 'large-push-2'; ?> medium-6 small-12 columns">
                                    <?php if( get_sub_field('quote_text') ): ?>
                                        <div class="content quote">
                                            <?php the_sub_field('quote_text'); ?>
                                        </div>
                                    <?php endif; ?>
                                    <?php if( get_sub_field('quote_author') ): ?>
                                        <div class="content author">
                                            <?php the_sub_field('quote_author'); ?>
                                        </div>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <?php $section_counter++; ?>
            <!-- Quote Section-->
        <?php elseif( get_row_layout() == 'usps_section' ): ?>
            <!-- Usps Section -->
            <section class="usps-section" id="section-<?php echo $section_counter; ?>">
                <?php if( have_rows('checkmarks') ): ?>
                    <div class="row">
                        <?php while( have_rows('checkmarks') ): the_row(); ?>
                            <div class="large-3 medium-6 small-12 columns">
                                <?php if( get_sub_field('title') ): ?>
                                    <?php $url = get_sub_field('url'); ?>
                                    <div class="inner-wrapper">
                                        <div class="checkbox-wrapper"></div>
                                        <h3 class="title <?php if($url): ?>link-title<?php endif; ?>"><?php the_sub_field('title'); ?></h3>
                                        <?php if( $url ): ?>
                                            <a href="<?php echo $url; ?>" class="link"></a>
                                        <?php endif; ?>
                                    </div>
                                <?php endif; ?>
                            </div>
                        <?php endwhile; ?>
                    </div>
                <?php endif; ?>
            </section>
            <?php $section_counter++; ?>
            <!-- End Usps Section -->
        <?php elseif( get_row_layout() == 'slider_section' ): ?>
            <!-- Slider Section-->
            <section class="slider-section" id="section-<?php echo $section_counter; ?>">
                <div class="row">
                    <div class="large-12 medium-12 small-12 columns">
                        <div class="pre-wrapper">
                            <?php if( get_sub_field('title') ): ?>
                                <div class="row">
                                    <div class="large-12 medium-12 small-12 columns">
                                        <h2 class="title"><?php the_sub_field('title'); ?></h2>
                                    </div>
                                </div>
                            <?php endif; ?>
                            <?php if( have_rows('slider_images') ): ?>
                                <div class="row">
                                    <div class="large-8 large-push-2 medium-10 medium-push-1 small-12 columns">
                                        <div class="main-slider slick-slider">
                                            <?php while( have_rows('slider_images') ): the_row(); ?>
                                                <?php if( $slider_image = get_sub_field('image') ): ?>                                                    
                                                    <div class="slick-slide">
                                                        <img src="<?php echo $slider_image['sizes']['slider-image']?>" alt="slider-image">
                                                    </div>
                                                <?php endif; ?>                                                
                                            <?php endwhile; ?>
                                        </div>                                        
                                    </div>                                        
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>                
            </section>
            <?php $section_counter++; ?>
            <!-- End Slider Section-->
        <?php elseif( get_row_layout() == 'video_section' ): ?>
            <!-- Video section -->
            <section class="video-section" id="section-<?php echo $section_counter; ?>">
                <div class="row">
                    <div class="large-12 medium-12 small-12 columns">
                        <div class="pre-wrapper">
                            <?php if( get_sub_field('title') ): ?>
                                <div class="row">
                                    <div class="large-12 medium-12 small-12 columns">
                                        <h2 class="title"><?php the_sub_field('title'); ?></h2>
                                    </div>
                                </div>
                            <?php endif; ?>
                            <?php if( get_sub_field('video') ):?>
                                <div class="row">
                                    <div class="large-8 large-push-2 medium-10 medium-push-1 small-12 columns">
                                        <div class="video-wrapper">
                                            <div class="video-container">
                                                <?php the_sub_field('video'); ?>
                                            </div>                                            
                                        </div>                                        
                                    </div>                                        
                                </div>
                            <?php endif; ?>
                        </div>                            
                    </div>                        
                </div>
            </section>
            <?php $section_counter++; ?>
            <!-- End Video section -->
        <?php elseif( get_row_layout() == 'images_section' ): ?>
            <section class="images_section" id="section-<?php echo $section_counter; ?>">
                <?php 
                    $class_option = '';
                    $images = get_sub_field('images'); 
                    $count_images = count($images);
                    if( $count_images == 2 ):
                        $class_option = 6;
                    elseif( $count_images == 3 ):
                        $class_option = 4;
                    elseif( $count_images == 4 ):
                        $class_option = 3;
                    endif;
                ?>
                <?php if( have_rows('images') ): ?>
                    <div class="row">
                        <?php while( have_rows('images') ): the_row(); ?>
                            <?php if( $image = get_sub_field('image') ): ?>
                                <div class="large-<?php echo $class_option; ?> medium-6 small-12 columns eq-image-cols">
                                    <div class="image-wrapper">
                                        <img src="<?php echo $image['sizes']['full_width_image'];?>" alt="image">
                                    </div>
                                </div>
                            <?php endif; ?>
                        <?php endwhile; ?>
                    </div>
                <?php endif; ?>
                <?php $section_counter++; ?>
            </section>
        <?php endif; ?>
    <?php endwhile; ?>
<?php endif; ?>



