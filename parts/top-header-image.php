<?php if( $image = get_field('top_header_image') ): ?>
<section class="top-header-section <?php if( !is_front_page() ): ?>default-page<?php endif; ?>">
    <div class="top-header-image" style="background-image: url(<?php echo $image['sizes']['full_width_image']; ?>)"></div>
    <?php if( !is_front_page() ): ?>
        <?php if( $text_slogan = get_field('top_text_slogan') ): ?>
            <div class="row">
                <div class="large-7 medium-9 small-10 columns">
                    <div class="slogan-wrapper">
                        <span class="slogan"><?php echo $text_slogan; ?></span>
                    </div>
                </div>
            </div>
        <?php else: ?>
            <?php $text_slogan = get_field('text_slogan', 'option'); ?>
            <div class="row">
                <div class="large-7 medium-9 small-10 columns">
                    <div class="slogan-wrapper">
                        <span class="slogan"><?php echo $text_slogan; ?></span>
                    </div>
                </div>
            </div>
        <?php endif; ?>
    <?php endif; ?>
</section>
<?php endif; ?>