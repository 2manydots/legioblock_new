<?php if( $home_top_content = get_field('content') ): ?>
    <!-- Home Page Top Section -->
    <section class="homepage-top-section">
        <div class="row">
            <div class="large-6 medium-12 small-12 columns left-section">
                <div class="content-wrapper">
                    <div class="content-inner-wrapper">
                        <?php if( get_field('title_top_content') ): ?>
                            <div class="title">
                                <?php the_field('title_top_content'); ?>
                            </div>
                        <?php endif;?>
                        <div class="content">
                            <?php the_field('content'); ?>                        
                        </div>
                        <?php 
                            $button_text = get_field('button_text');
                            $button_url  = get_field('button_url');
                        ?>
                        <?php if( $button_text && $button_url ): ?>
                            <a href="<?php echo $button_url; ?>" class="btn"><?php echo $button_text; ?></a>
                        <?php endif; ?>
                    </div>                    
                </div>
            </div>
            <div class="large-6 medium-12 small-12 columns no-padding">                
                <?php if( have_rows('right_blocks') ): ?>
                    <div class="right-blocks-wrapper">
                        <?php while( have_rows('right_blocks') ): the_row(); ?>
                            <div class="single-block">
                                <div class="inner-single-block">
                                    <?php if( $icon = get_sub_field('icon') ): ?>
                                        <img src="<?php echo $icon; ?>" alt="icon">
                                    <?php endif; ?>
                                    <?php if( $title = get_sub_field('title') ): ?>
                                        <h4><?php echo $title; ?></h4>
                                    <?php endif; ?>                                                                            
                                </div>
                                <?php if( $link = get_sub_field('link') ): ?>
                                    <a href="<?php echo $link; ?>"></a>
                                <?php endif; ?>
                            </div>
                        <?php endwhile; ?>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </section>
    <!-- End Home Page Top Section -->
<?php endif; ?>

