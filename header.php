<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7" <?php language_attributes(); ?>><![endif]-->
<!--[if IE 7]>   <html class="no-js lt-ie9 lt-ie8" <?php language_attributes(); ?>><![endif]-->
<!--[if IE 8]>   <html class="no-js lt-ie9" <?php language_attributes(); ?>><![endif]-->
<!--[if gt IE 8]><!--><html class="no-js" <?php language_attributes(); ?>><!--<![endif]-->
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php if ( !is_front_page() ) { wp_title(''); echo " - "; } bloginfo('name'); ?></title>
    <link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/images/favicon.png" />
    <?php wp_head(); ?>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-NKDQGKS');</script>
    <!-- End Google Tag Manager -->
</head>
<body <?php body_class('body-offset'); ?>>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NKDQGKS"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    <header class="header headroom">
        <div class="row">
            <div class="medium-12 columns">
                <a href="<?= home_url(); ?>" class="logo" rel="nofollow">
                    <img src="<?php echo get_template_directory_uri(); ?>/images/sprites/logo.svg" alt="logo">
                </a>
                <?php wp_nav_menu(array('menu_class' => 'main-nav', 'theme_location' => 'main-nav')) ?>
                <div class="menu-toggler">
                    <div class="menu-title">                                
                        <div class="nav-icon">
                          <span></span>
                          <span></span>
                          <span></span>
                        </div>
                    </div>
                </div>
                <div class="language-select" onclick="void(0)">
                    <?php if( have_rows('languages', 'option') ): ?>  
                        <?php while( have_rows('languages', 'option') ): the_row(); ?>
                            <?php 
                                $language_icon = get_sub_field('language_icon');
                                $link          = get_sub_field('link');
                                $this_site     = get_sub_field('this_site');
                            ?>
                            <?php if( $language_icon && $link && $this_site ): ?>
                                <a class="current-language" href="<?php echo $link; ?>">
                                    <img src="<?php echo $language_icon; ?>" alt="lang">
                                </a>
                            <?php endif; ?>
                        <?php endwhile; ?>
                    <?php endif; wp_reset_query(); ?>
                    <?php if( have_rows('languages', 'option') ): ?>
                        <ul class="other-languages">
                            <?php while( have_rows('languages', 'option') ): the_row(); ?>
                                <?php 
                                    $language_icon = get_sub_field('language_icon');
                                    $link          = get_sub_field('link');
                                    $this_site     = get_sub_field('this_site');
                                ?>
                                <?php if( $language_icon && $link && !$this_site ): ?>
                                    <li>
                                        <a href="<?php echo $link; ?>">
                                            <img src="<?php echo $language_icon; ?>" alt="lang">
                                        </a>
                                    </li>
                                <?php endif; ?>                                
                            <?php endwhile; ?>
                        </ul>                        
                    <?php endif; wp_reset_query(); ?>
                    <div class="arrow-select"></div>
                </div>                
                <div class="large-12 medium-12 small-12 columns">
                    <div class="mobile-menu">
                        <?php if( $header_button_text && $header_button_url ): ?>
                            <a href="<?php echo $header_button_url; ?>" class="btn header-btn">
                                <?php new Sprite('hand-right'); ?><?php echo $header_button_text; ?>
                            </a>
                        <?php endif; ?>
                        <?php wp_nav_menu(array('menu_class' => 'main-nav', 'theme_location' => 'mobile-nav', 'container_class' => 'main-nav-container', 'after'=>'<span class="arrow"></span>')); ?>
                    </div>
                </div>
            </div>
        </div>
    </header>