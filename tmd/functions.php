<?php

// Theme support
add_theme_support( 'menus' );
add_theme_support( 'post-thumbnails' );

// Custom image sizes
add_image_size( 'full_width_image', 1920, 0, false );
add_image_size( 'full_width_image_2', 1920, 0, false );
add_image_size( 'contact-advisor-thumb', 170, 210, true );
add_image_size( 'ctb-thumb', 370, 217, true );
add_image_size( 'slider-image', 750, 410, true );

// add_image_size ( string $name, int $width, int $height, bool|array $crop = false )

// Navigations
function tmd_register_nav_menus() {
    //'footer-nav' => 'Footer navigation',
    //'top-nav' => 'Top navigation',
    $locations = array(        
        'main-nav' => 'Main navigation',        
        'mobile-nav' => 'Mobile navigation',
    );
    register_nav_menus( $locations );

}
add_action( 'init', 'tmd_register_nav_menus' );

// Sidebars
function tmd_register_sidebars() {

    $args = array(
        'id'            => 'sidebar-left',
        'name'          => 'Sidebar left',
        'description'   => 'Description',
    );
    register_sidebar( $args );

    $args = array(
        'id'            => 'sidebar-right',
        'name'          => 'Sidebar right',
        'description'   => 'Description',
    );
    register_sidebar( $args );

}
add_action( 'widgets_init', 'tmd_register_sidebars' );

// clear dashboard
function remove_menus() {
  remove_menu_page('edit-comments.php');
  remove_menu_page('edit.php');
}
add_action('admin_menu', 'remove_menus');

// clear wp admin bar
function remove_wp_nodes() {
  global $wp_admin_bar;
  $wp_admin_bar->remove_node('new-post');
  $wp_admin_bar->remove_node('comments');
}
add_action('admin_bar_menu', 'remove_wp_nodes', 999);

// Create pagination
if (!function_exists('foundation_pagination')) :
    function foundation_pagination($the_query = false) {
        if($the_query) {
            $wp_query = $the_query;
        } else {
            global $wp_query;
        }

        $big = 999999999; // This needs to be an unlikely integer

        // For more options and info view the docs for paginate_links()
        // http://codex.wordpress.org/Function_Reference/paginate_links
        $paginate_links = paginate_links( array(
            'base' => str_replace( $big, '%#%', html_entity_decode( get_pagenum_link( $big ) ) ),
            'current' => max( 1, get_query_var( 'paged' ) ),
            'total' => $wp_query->max_num_pages,
            'mid_size' => 5,
            'prev_next' => true,
            'prev_text' => __( '&laquo;', 'foundationpress' ),
            'next_text' => __( '&raquo;', 'foundationpress' ),
            'type' => 'list',
        ) );

        $paginate_links = str_replace( "<ul class='page-numbers'>", "<ul class='pagination'>", $paginate_links );
        $paginate_links = str_replace( '<li><span class="page-numbers dots">', "<li><a href='#'>", $paginate_links );
        $paginate_links = str_replace( "<li><span class='page-numbers current'>", "<li class='current'><a href='#'>", $paginate_links );
        $paginate_links = str_replace( '</span>', '</a>', $paginate_links );
        $paginate_links = str_replace( "<li><a href='#'>&hellip;</a></li>", "<li><span class='dots'>&hellip;</span></li>", $paginate_links );
        $paginate_links = preg_replace( '/\s*page-numbers/', '', $paginate_links );

        // Display the pagination if more than one page is found.
        if ( $paginate_links ) {
            echo '<div class="pagination-centered">';
            echo $paginate_links;
            echo '</div>';
        }
    }

endif;

// Theme Settings page
if (function_exists('acf_add_options_page')) {

  acf_add_options_page(array(
      'page_title' => 'Theme Settings',
      'menu_title' => 'Theme Settings',
      'menu_slug' => 'theme-settings',
      'capability' => 'edit_posts',
      'redirect' => false
  ));
}

// button shortcode
// [button link=""]text[/button]
function button_func($atts, $content = null) {


  $attr = shortcode_atts(array(
      'link' => NULL,
          ), $atts);

  $source = $attr['link'];
  $source = '<a href="' . $attr['link'] . '" class="btn"><span>' . $content . '</span></a>';
  return $source;
}
add_shortcode('button', 'button_func'); 

// custom excerpt
function custom_length_excerpt($word_count_limit) {
    $content = wp_strip_all_tags(get_the_content() , true );
    echo wp_trim_words($content, $word_count_limit);
}
// filter the Gravity Forms button type
add_filter( 'gform_submit_button', 'form_submit_button', 10, 2 );
function form_submit_button( $button, $form ) {
    return "<button class='btn gform_button' id='gform_submit_button_{$form['id']}'>{$form['button']['text']}</button>";
}
//Order Post Type
add_post_type_support( 'project', 'page-attributes' );
//Populate ACF Select field from ACF options page
function acf_select_popup( $field ) {
    
    // reset choices
    $field['choices'] = array();


    // if has rows
    if( have_rows('popups', 'option') ) {
        $count = 0;
        // while has rows
        while( have_rows('popups', 'option') ) {
            
            // instantiate row
            the_row();
            
            
            // vars
            $value = $count;
            $label = get_sub_field('title');

            
            // append to choices
            $field['choices'][ $value ] = $label;
            
            $count++;
        }
        
    }


    // return the field
    return $field;
    
}

add_filter('acf/load_field/name=select_popup', 'acf_select_popup');