<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage tmd-wp-grunt
 * @since 0.1.0
 */

get_header(); ?>

    <main class="main">

        <?php get_template_part('parts/breadcrumbs'); ?>

        <div class="row">
            <div class="medium-12 columns">

                <?php if ( have_posts() ) : ?>

                    <?php while ( have_posts() ) : the_post(); ?>

                        <div class="row">
                            <div class="medium-12 columns">
                                <h1><?php the_title(); ?></h1>
                                <?php the_content(); ?>
                            </div>
                        </div>

                        <?php if ( comments_open() || get_comments_number() ) : ?>
                        <div class="row">
                            <div class="medium-12 columns">
                                <?php //comments_template(); ?>
                            </div>
                        </div>
                        <?php endif;

                    endwhile;

                    get_template_part('parts/pagination');

                else :

                    get_template_part('parts/no-content');

                endif; ?>

            </div>
        </div>
    </main>

<?php get_footer(); ?>