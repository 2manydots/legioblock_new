<?php if( get_field('enable_click_through_block') ): ?>
    <!-- Click Through Blocks -->
    <section class="click-through-blocks">
        <?php if( get_field('click_through_blocks_title') ): ?>
            <div class="row">
                <div class="large-12 medium-12 small-12 columns">
                    <h3 class="section-title"><?php the_field('click_through_blocks_title'); ?></h3>
                </div>
            </div>
        <?php endif; ?>
        <?php if( have_rows('click_through_blocks') ): ?>
            <div class="row blocks-columns">
                <?php while( have_rows('click_through_blocks') ): the_row(); ?>
                    <div class="large-4 medium-6 small-12 columns column-wrapper">
                        <?php 
                            $custom_title = get_sub_field('title');
                            $custom_image = get_sub_field('image');
                        ?>
                        <?php $page_post_object = get_sub_field('page_post_object'); ?>                        
                        <?php if( $page_post_object ): $post = $page_post_object; setup_postdata( $post ); ?>
                            <div class="inner-wrapper">
                                <div class="image-wrapper">
                                    <?php if( $custom_image ): ?>
                                        <img src="<?php echo $custom_image['sizes']['ctb-thumb']; ?>" alt="image">
                                    <?php else: ?>
                                        <?php if( has_post_thumbnail() ): ?>
                                            <?php the_post_thumbnail('ctb-thumb'); ?>
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </div>
                                <div class="title-wrapper">
                                    <?php if( $custom_title ): ?>
                                        <h4 class="small-title"><?php echo $custom_title; ?></h4>
                                    <?php else: ?>
                                        <h4 class="small-title"><?php the_title(); ?></h4>
                                    <?php endif; ?>
                                </div>
                                <a href="<?php the_permalink(); ?>"class="link"></a>
                            </div>
                        <?php wp_reset_postdata(); endif; ?>
                    </div>
                <?php endwhile; ?>
            </div>
        <?php endif; ?>
    </section>
    <!-- End Click Through Blocks -->
<?php endif; ?>
<footer class="main-footer">
    <div class="row">
        <?php if( have_rows('footer_columns','option') ): ?>
            <?php while( have_rows('footer_columns','option') ): the_row(); ?>
                <?php 
                    $add_socials = get_sub_field('add_socials'); 
                    $add_menu    = get_sub_field('add_menu');
                ?>
                <div class="large-3 medium-6 small-12 columns footer-column">
                    <?php if( $title = get_sub_field('title') ): ?>
                        <h3><?php echo $title; ?></h3>
                    <?php endif; ?>
                    <?php if( !$add_socials ): ?>
                        <?php if( $content = get_sub_field('content') ): ?>
                            <div class="content">
                                <?php echo $content; ?>
                            </div>
                        <?php endif; ?>
                    <?php else: ?>
                        <div class="socails">
                            <?php if( get_sub_field('linkedin_link') ): ?>
                                <a href="<?php the_sub_field('linkedin_link'); ?>" class="linkedin" target="_blank"></a>
                            <?php endif; ?>
                            <?php if( get_sub_field('facebook_link') ): ?>
                                <a href="<?php the_sub_field('facebook_link'); ?>" class="facebook" target="_blank"></a>
                            <?php endif; ?>
                        </div>                        
                    <?php endif; ?>
                    <?php if( $add_menu ): ?>
                        <?php if( $select_menu_id = get_sub_field('select_menu') ): ?>
                            <?php wp_nav_menu(array(
                                    'menu' => $select_menu_id,
                                    'menu_class' => 'footer-menu',                                    
                                    'container' => ''
                                    )); ?>
                        <?php endif; ?>
                    <?php endif; ?>
                </div>
            <?php endwhile; ?>
        <?php endif; ?>         
    </div>
</footer>
<?php if( get_field('show_pop_up') ): ?>
    <?php ?>
    <?php if( $select_popup = get_field_object('select_popup') ): ?>
        <?php 
            $value = $select_popup['value'];
            $rows= get_field('popups', 'option');
            $seleted_row = $rows[$value];

            //echo $seleted_row['title'];
            $seleted_row['title'];
            $seleted_row['content'];          
        ?>
        <div class="pop-up"
            <?php if( get_field('select_pop_appearance') == 1 ): ?>
                <?php if(get_field('timeout_show')):?>data-timeout="<?php echo the_field('timeout_show'); ?>"<?php endif; ?>
            <?php elseif( get_field('select_pop_appearance') == 2 ): ?>
                <?php if(get_field('scrolled_pixels_from_top')):?>data-pxftop="<?php echo the_field('scrolled_pixels_from_top'); ?>"<?php endif; ?>
            <?php endif; ?>                        
        >
            <div class="close-btn"></div>
            <div class="inner-wrapper">
                <?php echo $seleted_row['content']; ?>
            </div>
        </div>
    <?php endif; ?>
<?php endif;?>
<?php wp_footer(); ?>
</body>
</html>