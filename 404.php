<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @package WordPress
 * @subpackage tmd-wp-grunt
 * @since Grunt Boilerplate 0.1.0
 * @author 2manydots
 */

get_header(); ?>

<main class="main non-page">
    <?php if( $post_object_404 = get_field('select_404_page', 'option') ): $post = $post_object_404; setup_postdata( $post ); ?>
        <?php get_template_part('parts/top-header-image'); ?>
            <div class="row">
                <div class="large-7 medium-9 small-10 columns">
                    <div class="breadcrumbs" typeof="BreadcrumbList" vocab="https://schema.org/">
                        <?php if(function_exists('bcn_display')):                            
                            bcn_display();
                        endif; ?>
                    </div>
                </div>                        
            </div>
        <?php if( get_field('intro_text') ): ?>
            <div class="row">
                <div class="large-8 large-push-2 medium-10 medium-push-1 small-12 columns">
                    <div class="intro-text"><?php the_field('intro_text'); ?></div>
                </div>
            </div>
        <?php endif; ?>    
        <div class="row">
            <div class="large-8 large-push-2 medium-10 medium-push-1 small-12 columns">
                <form role="search" method="get" id="searchform" class="searchform" action="<?php bloginfo('url') ?>">
                    <div>
                        <label class="screen-reader-text" for="s">Zoek naar:</label>
                        <input type="text" value="" name="s" id="s">
                        <button class="btn btn_round" id="searchsubmit">Zoeken</button>
                    </div>
                </form>
            </div>
        </div>
        <?php get_template_part('parts/flexible-sections'); ?>
    <?php wp_reset_postdata(); endif; ?>
    
</main>

<?php get_footer(); ?>
