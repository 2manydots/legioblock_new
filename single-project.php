<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage tmd-wp-grunt
 * @since 0.1.0
 */
get_header(); ?>

    <main class="main">
        
        <?php if ( have_posts() ) : ?>
            <?php while ( have_posts() ) : the_post(); ?>
                <?php get_template_part('parts/top-header-image'); ?>
                <div class="row">
                    <div class="large-7 medium-9 small-12 columns">
                        <div class="breadcrumbs" typeof="BreadcrumbList" vocab="https://schema.org/">
                            <?php if(function_exists('bcn_display')):
                                bcn_display();
                            endif; ?>
                        </div>                        
                    </div>
                    <div class="large-5 medium-12 small-12 columns">
                        <div class="meta-row">
                            <?php $terms_category = wp_get_post_terms( get_the_ID(), 'project_category'); ?>
                            <?php if( $terms_category ): ?>
                                <?php foreach( $terms_category as $term_category ): ?>
                                    <?php $category_term_ID = $term_category->term_id; ?>
                                <?php endforeach; ?>
                                <div class="category meta-block">                                    
                                    <div class="category-image">
                                        <?php if( get_field('image', 'project_category' . '_' . $category_term_ID) ): ?>
                                            <img src="<?php the_field('image', 'project_category' . '_' . $category_term_ID); ?>" alt="icon">
                                        <?php endif; ?>
                                    </div>                                    
                                    <?php foreach( $terms_category as $term_category ): ?>
                                        <span class="text">
                                            <?php echo $term_category->name; ?>
                                        </span>                                        
                                    <?php endforeach; ?>
                                </div>
                            <?php endif; ?>                                                        
                            <?php $terms_tags = wp_get_post_terms( get_the_ID(), 'project_tag'); ?>                            
                            <?php if( $terms_tags ): ?>
                                <?php foreach( $terms_tags as $term_tags ): ?>
                                    <?php $tag_term_ID = $term_tags->term_id;?>
                                <?php endforeach; ?>
                                <div class="tags meta-block">
                                    <div class="tag-image">
                                        <?php if( get_field('image', 'project_tag' . '_' . $tag_term_ID) ): ?>
                                            <img src="<?php the_field('image', 'project_tag' . '_' . $tag_term_ID); ?>" alt="icon">
                                        <?php endif; ?>
                                    </div>
                                    <?php 
                                        $terms_tags_count = count($terms_tags); 
                                        $terms_tags_i = 1;
                                    ?>                                    
                                    <?php foreach( $terms_tags as $term_tags ): ?>
                                        <span class="text">
                                            <?php 
                                                echo $term_tags->name;
                                                if( $terms_tags_i < $terms_tags_count ):
                                                    echo ',';
                                                endif;
                                            ?>
                                        </span>                                        
                                    <?php $terms_tags_i++; endforeach; ?>
                                </div>                                
                            <?php endif; ?>                                                        
                            <!--<div class="date meta-block">
                                <span class="text"><?php echo get_the_time('Y'); ?></span>
                            </div>-->
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="large-8 large-push-2 medium-10 medium-push-1 small-12 columns">
                        <h1><?php the_title(); ?></h1>
                    </div>
                </div>
                <?php if( get_field('intro_text') ): ?>
                    <div class="row">
                        <div class="large-8 large-push-2 medium-10 medium-push-1 small-12 columns">
                            <div class="intro-text"><?php the_field('intro_text'); ?></div>
                        </div>
                    </div>
                <?php endif; ?>
                <?php get_template_part('parts/flexible-sections'); ?>
            <?php endwhile; ?>
        <?php endif; ?>
        
    </main>

<?php get_footer(); ?>