<?php
/**
 * The template for displaying default pages
 *
 * @package WordPress
 * @subpackage tmd-wp-grunt
 * @since 0.1.0
 */

get_header(); ?>

    <main class="main">
        
        <?php if ( have_posts() ) : ?>
            <?php while ( have_posts() ) : the_post(); ?>
                <?php get_template_part('parts/top-header-image'); ?>
                <div class="row">
                    <div class="large-7 medium-9 small-10 columns">
                        <div class="breadcrumbs" typeof="BreadcrumbList" vocab="https://schema.org/">
                            <?php if(function_exists('bcn_display')):                            
                                bcn_display();
                            endif; ?>
                        </div>
                    </div>                        
                </div>
                <div class="row">
                    <div class="large-8 large-push-2 medium-10 medium-push-1 small-12 columns">
                        <!--<h1><?php the_title(); ?></h1>-->
                    </div>
                </div>
                <?php if( get_field('intro_text') ): ?>
                    <div class="row">
                        <div class="large-8 large-push-2 medium-10 medium-push-1 small-12 columns">
                            <div class="intro-text"><?php the_field('intro_text'); ?></div>
                        </div>
                    </div>
                <?php endif; ?>
                <?php get_template_part('parts/flexible-sections'); ?>
            <?php endwhile; ?>
        <?php endif; ?>
        
    </main>

<?php get_footer(); ?>